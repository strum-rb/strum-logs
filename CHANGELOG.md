## [Unreleased]

## [1.0.6] - 2023-07-14
### Added
- Session logging [@dmytro.pivtorak].[QSS-1169](https://employeesgate.atlassian.net/browse/QSS-1169)

## [0.1.0] - 2022-07-28

- Initial release
