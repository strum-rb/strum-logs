# frozen_string_literal: true

require "httparty"

module HTTParty
  module ClassMethods
    def perform_request(http_method, path, options, &block)
      options = ModuleInheritableAttributes.hash_deep_dup(default_options).merge(options)
      HeadersProcessor.new(headers, options).call
      process_cookies(options)
      response = Request.new(http_method, path, options).perform(&block)
      StrumLogs::Configuration.config.httparty_after_call_hooks.each { |hook| hook.call(http_method, path, options, response) }
      response
    end
  end
end
