require "strum_logs/helpers/filters"

module StrumLogs
  module Filters
    class HttpFilter
      include StrumLogs::Helpers::Filters

      def filter!(log_entity)
        filter_query(log_entity) unless log_entity[:query].nil?
        filter_headers(log_entity)
        filter_response_message(log_entity)
        log_entity
      end

      private

      def filter_query(log_entity)
        log_entity[:query] = URI.decode_www_form(log_entity[:query]).to_h.map do |key, val|
          should_filter?(key) ? "#{key}=#{REPLACED_VALUE}" : "#{key}=#{val}"
        end.join('&')
      end

      def filter_response_message(log_entity)
        parsed_body = parse_json(log_entity[:response_message])

        case parsed_body
        when Hash
          log_entity[:response_message] = filter_hash(parsed_body).to_json
        when Array
          log_entity[:response_message] = filter_array(parsed_body).to_json
        else
          # type code here
        end
      end

      def filter_headers(log_entity)
        log_entity[:headers] = filter_hash(log_entity[:headers])
      end
    end
  end
end
