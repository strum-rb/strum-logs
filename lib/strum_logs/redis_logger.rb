# frozen_string_literal: true

require "strum_logs/default_logger"
require "redis"

module StrumLogs
  module Redis
    class Logger
      def initialize
        @logger = StrumLogs::DefaultLogger.instance.logger
      end

      def call(command, reply)
        logs = { request: command.join(" "), message: "redis request" }
        @logger.error(logs.merge({ error: reply })) && return if reply.is_a?(::Redis::CommandError)

        @logger.info(logs.merge({ response_message: reply }))
      end
    end
  end
end
