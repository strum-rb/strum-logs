# frozen_string_literal: true

require "strum_logs/helpers/http_log_helper"
require "strum_logs/helpers/session_log_helper"

module StrumLogs
  module Httparty
    class Logger
      include StrumLogs::Helpers::HttpLogHelper
      include StrumLogs::Helpers::SessionLogHelper

      def initialize
        @logger = DefaultLogger.instance.logger
        @tracer = DefaultTelemetry.instance.tracer
      end

      def call(http_method, path, options, response)
        @tracer.in_span(path.to_s, kind: :client) do |span|
          logging_process(http_method, path, options, response, span)
        end
      end

      def logging_process(http_method, path, options, response, span)
        log_entity = init_log(http_method, path, options)
        log_entity[:status] = response.code
        log_status(log_entity, response)
        log_data(log_entity, response)
        push_session_log(log_entity, options[:headers])
        tracer_info_set(response) if Configuration.config.enable_export_spans
      rescue StandardError => e
        error_process(log_entity, e, span)
        raise e
      ensure
        logg_process(log_entity, response)
      end

      def init_log(http_method, path, options)
        {
          method: http_method.to_s.split("::")[2].upcase,
          path: path.to_s,
          query: options[:query],
          protocol: "HTTP",
          started_at: Time.now,
          message: "Httparty Request"
        }
      end

      def log_status(log_entity, response)
        response.success? ? log_entity.merge!({ log_status: "success" }) : log_entity.merge!({ log_status: "error" })
      end

      def log_data(log_entity, response)
        log_entity[:status] = response.code
        log_entity[:headers] = response.headers
        log_entity[:response_message] = parse_body(response.body)
      end

      def parse_body(body)
        case body
        when Hash
          body
        when Array
          body.first
        else
          body
        end
      end

      def tracer_info_set(response)
        span = OpenTelemetry::Trace.current_span(OpenTelemetry::Context.current)
        context = span.context
        response.headers[:trace_id] = context.hex_trace_id
        response.headers[:span_id] = context.hex_span_id
      end

      def error_process(log_entity, error, span)
        log_entity[:error] = error.message
        log_entity[:stack_trace] = error.backtrace if Configuration.config.stack_trace
        span.record_exception(error)
      end

      def logg_process(log_entity, response)
        log_entity[:request] = build_request_hash(response.request)
        log_entity[:elapsed_ms] = elapsed_ms(log_entity)
        output(log_entity)
      end

      def build_request_hash(request)
        {
          path: request.path.to_s,
          body: request.raw_body
        }
      end
    end
  end
end
