# frozen_string_literal: true

require "singleton"
module StrumLogs
  class DefaultLogger
    include Singleton

    attr_reader :logger

    def initialize
      @logs_filter = init_filter
      @logger = init_log_system
    end

    def self.logger
      instance.logger
    end

    private

    def init_log_system # rubocop:disable Metrics/AbcSize
      $stdout.sync = true if Configuration.config.stdout_sync
      logger = Ougai::Logger.new($stdout)

      logger.before_log = -> (data) do
        unless @logs_filter.nil?
          @logs_filter.filter_http_log(data) if data[:protocol] == "HTTP" # TODO: test protocol
          @logs_filter.filter_esb_log(data) if data[:protocol] == "AMQP"
          # @logs_filter.filter_redis_log(data)
          # @logs_filter.filter_sequel_log(data)
        end
        tracer_info_set(data) if Configuration.config.enable_export_spans
      end
      logger.level = Configuration.config.level
      logger.formatter = Ougai::Formatters::Readable.new unless %w[prod production
                                                                   test].include?(Configuration.config.environment)
      logger.with_fields = { service_name: Configuration.config.application_name,
                             version: Configuration.config.application_version,
                             team: Configuration.config.team_name }
      logger
    end

    def tracer_info_set(data)
      span = OpenTelemetry::Trace.current_span(OpenTelemetry::Context.current)
      context = span.context
      data[:trace_id] = context.hex_trace_id
      data[:span_id] = context.hex_span_id
    end

    def init_filter
      StrumLogs::Filter.new unless Configuration.config.black_list_keys == []
    end
  end
end
