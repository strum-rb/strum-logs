module StrumLogs
  module Helpers
    module Filters
      REPLACED_VALUE = "****".freeze # can move replaced value to config

      def filter_array(array)
        array.each_with_index do |element, index|
          case element
          when Hash
            filter_hash(array[index])
          when Array
            filter_array(array[index])
          else
            next
          end
        end
      end

      def filter_hash(hash)
        hash.each_key do |key|
          if should_filter?(key.to_s)
            hash[key] = REPLACED_VALUE
          elsif hash[key].is_a?(Hash)
            filter_hash(hash[key])
          elsif hash[key].is_a?(Array)
            hash[key].each_with_index do |h, i|
              hash[key][i] = filter_hash(h)
            end
          end
        end
      end

      def should_filter?(key)
        Configuration.config.black_list_keys.include? key.downcase
      end

      def parse_json(data)
        JSON.parse data
      rescue JSON::ParserError, TypeError => _e
        return data
      end
    end
  end
end
