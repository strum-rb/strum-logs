# frozen_string_literal: true

module StrumLogs
  module Helpers
    module SessionLogHelper
      def push_session_log(log_entity, env)
        log_entity[:session_key] = env["HTTP_SESSION"] if env && env["HTTP_SESSION"]
      end
    end
  end
end
