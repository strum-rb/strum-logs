# frozen_string_literal: true

require "strum_logs/default_logger"

module StrumLogs
  module Strum
    class Logger
      def initialize(context = nil)
        @logger = StrumLogs::DefaultLogger.instance.logger
        @context = context
        @trace_id = generate_random_sequence(30)
      end

      def generate_random_sequence(length)
        [*"a".."z", *0..9, *"A".."Z"].shuffle[0..length].join
      end

      def log(inputs, outputs, errors, step_name)
        default_log = init_log(inputs, step_name)
        errors.empty? ? @logger.info(success_log(default_log, outputs)) : @logger.error(failure_log(default_log, errors))
      rescue StandardError => e
        @logger.error(default_log.merge({ log_status: "error", error: e.message, stack_trace: e }))
      end

      def init_log(inputs, step_name)
        {
          request: inputs,
          message: "Strum pipeline step",
          path: step_name,
          trace_id: @trace_id,
          context: @context,
          internal: true
        }
      end

      def success_log(default_log, outputs)
        default_log.merge({ log_status: "success", response_message: outputs })
      end

      def failure_log(default_log, errors)
        default_log.merge({ log_status: "error", error: errors })
      end
    end
  end
end
