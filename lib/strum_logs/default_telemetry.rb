# frozen_string_literal: true

require "singleton"
module StrumLogs
  class DefaultTelemetry
    include Singleton

    attr_reader :tracer

    def initialize
      @tracer = init_tracer
    end

    def self.tracer
      instance.tracer
    end

    private

    def init_tracer
      open_telemetry_config
      OpenTelemetry.tracer_provider.tracer(Configuration.config.application_name,
                                           Configuration.config.application_version)
    end

    def open_telemetry_config # rubocop:disable  Metrics/CyclomaticComplexity,Metrics/AbcSize
      ENV["OTEL_TRACES_EXPORTER"] = "none" if !Configuration.config.enable_export_spans && !ENV.key?("OTEL_TRACES_EXPORTER")
      OpenTelemetry::SDK.configure do |c|
        c.service_name = Configuration.config.application_name
        c.service_version = Configuration.config.application_version
        c.use "OpenTelemetry::Instrumentation::PG" if Configuration.config.pg_instrumentation
        c.use "OpenTelemetry::Instrumentation::Redis" if Configuration.config.redis_instrumentation
        c.use "OpenTelemetry::Instrumentation::Rack" if Configuration.config.rack_instrumentation
        c.use "OpenTelemetry::Instrumentation::Faraday" if Configuration.config.faraday_instrumentation
        c.use "OpenTelemetry::Instrumentation::Bunny" if Configuration.config.rabbit_instrumentation
      end
    end
  end
end
