require "strum_logs/filters/http_filter"
require "strum_logs/filters/esb_filter"

module StrumLogs
  class Filter

    AVAILABLE_FILTER_PARAM_TYPES = [String]

    def initialize
      @filter_keys = Configuration.config.black_list_keys
      validate_configuration

      @http_filter = StrumLogs::Filters::HttpFilter.new # can add array which filter we need to use
      @amqp_filter = StrumLogs::Filters::EsbFilter.new
    end

    def filter_http_log(log_entity)
      @http_filter.filter!(log_entity)
    end

    def filter_esb_log(log_entity)
      @amqp_filter.filter!(log_entity)
    end

    private

    def validate_configuration
      unless @filter_keys.is_a? Array
        raise StrumLogs::Errors::Configuration.new "Configuration black_list_keys type error. Value must be Array but received #{@filter_keys.class}"
      end

      @filter_keys.each do |filter_parameter|
        unless AVAILABLE_FILTER_PARAM_TYPES.include? filter_parameter.class
          raise StrumLogs::Errors::Filtration.new "Filter parameter type error. Available parameter type #{AVAILABLE_FILTER_PARAM_TYPES}"
        end
      end
    end
  end
end
