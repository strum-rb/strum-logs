# frozen_string_literal: true

require "spec_helper"

describe StrumLogs::Rack::RequestLogMiddleware do
  let(:env) { Rack::MockRequest.env_for }
  describe "when successful request" do
    let(:app) { ->(_env) { [200, {}, "success"] } }
    it "creates logs for success response" do

      response, logs = logs_response { StrumLogs::Rack::RequestLogMiddleware.new(app).call(env) }
      _(response.first).must_equal(200)
      _(response[2]).must_equal("success")
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("success")
      _(logs["team"]).must_equal("test")
    end

    it "creates logs for success response with session" do
      env["HTTP_SESSION"] = "session-key"
      response, logs = logs_response { StrumLogs::Rack::RequestLogMiddleware.new(app).call(env) }
      _(response.first).must_equal(200)
      _(response[2]).must_equal("success")
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("success")
      _(logs["team"]).must_equal("test")
      _(logs["session_key"]).must_equal("session-key")
    end
  end
end
