# frozen_string_literal: true

require "spec_helper"

describe StrumLogs::Faraday::RequestLogMiddleware do
  let(:faraday_stubs) { Faraday::Adapter::Test::Stubs.new }
  let(:faraday) do
    Faraday.new(headers: {}) do |faraday|
      faraday.adapter :test, faraday_stubs
      faraday.use StrumLogs::Faraday::RequestLogMiddleware
    end
  end
  describe "when successful request with faraday" do
    it "creates logs for success response" do
      body = { :password => "qwerty",
               :hash_one => {
                 :password => "qwerty",
                 :array => [{ :password => "qwerty" }],
                 :hash_to => {
                   :password => "qwerty",
                   :hash_three => {
                     :password => "qwerty"
                   }
                 }
               }
      }
      faraday_stubs.get("https://example.org/url") { [200, {}, body.to_json] }
      _response, logs = logs_response { faraday.get("https://example.org/url") }
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("{\"password\":\"****\",\"hash_one\":{\"password\":\"****\",\"array\":[{\"password\":\"****\"}],\"hash_to\":{\"password\":\"****\",\"hash_three\":{\"password\":\"****\"}}}}")
      _(logs["team"]).must_equal("test")
    end

    it "creates logs with array body for success response" do
      body = [{ password: "qwerty" }, [{ password: "qwerty" }], [[{ password: "qwerty" }]]]
      faraday_stubs.get("https://example.org/url") { [200, {}, body.to_json] }
      _response, logs = logs_response { faraday.get("https://example.org/url") }
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("[{\"password\":\"****\"},[{\"password\":\"****\"}],[[{\"password\":\"****\"}]]]")
      _(logs["team"]).must_equal("test")
    end

    it "creates logs with query for success response" do
      faraday_stubs.get("https://example.org/url?password=here") { [200, {}, {}.to_json] }
      _response, logs = logs_response { faraday.get("https://example.org/url?password=here") }
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("{}")
      _(logs["query"]).must_equal("password=****")
      _(logs["team"]).must_equal("test")
    end

    it "creates logs with headers for success response" do
      faraday_stubs.get("https://example.org/url") { [200, { "Password" => "qwerty" }, {}] }
      _response, logs = logs_response { faraday.get("https://example.org/url") }
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("GET")
      _(logs["headers"]).must_equal({ "Password" => "****" })
      _(logs["team"]).must_equal("test")
    end

    it "creates logs with session header for success response" do
      faraday_stubs.get("https://example.org/url") { [200, { "Password" => "qwerty" }, {}] }
      _response, logs = logs_response { faraday.get("https://example.org/url", {}, {"HTTP_SESSION" => "session-key"} ) }
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["session_key"]).must_equal("session-key")
      _(logs["method"]).must_equal("GET")
      _(logs["headers"]).must_equal({ "Password" => "****" })
      _(logs["team"]).must_equal("test")
    end
  end
end
